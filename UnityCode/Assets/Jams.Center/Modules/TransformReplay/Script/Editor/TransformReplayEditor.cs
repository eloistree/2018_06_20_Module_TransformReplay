﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TransformReplay))]
[CanEditMultipleObjects]

public class TransformReplayEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        TransformReplay replay = (TransformReplay)target;

        EditorGUILayout.BeginHorizontal();

        if (replay.m_isPlaying &&  GUILayout.Button("Play"))
        {
            replay.Play();
        }
        if (!replay.m_isPlaying && GUILayout.Button("Stop"))
        {
            replay.Stop();
        }
        if (GUILayout.Button("Reload"))
        {
            replay.m_framesRecord.Load();
        }
        if (GUILayout.Button("Normalized"))
        {
            replay.m_framesRecord.Normalized();
        }
       

        //if (GUILayout.Button("Normalized"))
        //{
        //    replay.m_framesRecord.Normalized();
        //}

        EditorGUILayout.EndHorizontal();
    }

}
