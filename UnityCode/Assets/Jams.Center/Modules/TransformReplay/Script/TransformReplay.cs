﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Events;

public class TransformReplay : MonoBehaviour
{

    public Transform m_affected;
    public FramesRecord m_framesRecord;
    private FrameTransform m_from = new FrameTransform();
    private FrameTransform m_to = new FrameTransform();

    [Header("Events")]
    public UnityEvent m_onFinish;



    [Header("Debug (Don't Touch)")]
    public bool m_isPlaying;
    public float m_time;


    [Header("Load Replay")]
    public TextAsset m_jsonSave;



   

    void Update()
    {
        if (!m_isPlaying) return;


        float oldTime = m_time;
        m_time += Time.deltaTime;


        float time = GetTime();
        var list = m_framesRecord.m_frames
            .Where(k => k.m_time <= time)
            .OrderBy(k => Mathf.Abs(k.m_time - time))
            .ToList();
        if (list.Count > 0)
            m_from = list[0];

        list = m_framesRecord.m_frames
            .Where(k => k.m_time >= time)
            .OrderBy(k => Mathf.Abs(k.m_time - time))
            .ToList();
        if (list.Count > 0)
            m_to = list[0];

        if (m_from != null && m_to != null && m_from.m_time != m_to.m_time)
        {

            float pct = (time - m_to.m_time) / (m_from.m_time - m_to.m_time);

            m_affected.position = Vector3.Lerp(m_from.m_position, m_to.m_position, 1f - pct);
            m_affected.rotation = Quaternion.Lerp(m_from.m_rotation, m_to.m_rotation, 1f - pct);
        }
        float endReplay = m_framesRecord.GetDuration();
        if (oldTime < endReplay && m_time>= endReplay ) {
            m_onFinish.Invoke();
        }


    }
    public float GetTime()
    {
        return m_time;
    }

    public void OnValidate()
    {
        if (m_jsonSave != null)
        {

            m_framesRecord = JsonUtility.FromJson<FramesRecord>(m_jsonSave.text);
            m_jsonSave = null;
        }
        
        m_framesRecord.ConvertAsValide();
    }

    public void Play()
    {

        m_isPlaying = true;
    }

    public void Stop()
    {

        m_isPlaying = false;
    }

    public void Reset()
    {
        m_affected = transform;
        m_time = 0;
    }
}
